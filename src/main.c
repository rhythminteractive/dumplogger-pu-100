/**
 * An Mirf example which copies back the data it recives.
 *
 * Pins:
 * Hardware SPI:
 * MISO -> 12
 * MOSI -> 11
 * SCK -> 13
 *
 * Configurable:
 * CE -> 8
 * CSN -> 7
 *
 */


#define CODE_AVAILABLE 2
#define CODE_UNAVAILABLE 1
#define CODE_CALIBRATING 5
#define PIN_RGB_R 3
#define PIN_RGB_G 5
#define PIN_RGB_B 6

#include <SPI.h>
#include <Mirf.h>
#include <nRF24L01.h>
#include <Bridge.h>
#include <Process.h>
#include <MirfHardwareSpiDriver.h>


Process hipChatProcess;
void setup() {
    Serial.begin(9600);
    
    /*
     * Set the SPI Driver.
     */
    
    Mirf.cePin = 9;
    Mirf.csnPin = 10;
    
    
    Mirf.spi = &MirfHardwareSpi;
    /*
     * Setup pins / SPI.
     */
    
    Mirf.init();
    
    /*
     * Configure reciving address.
     */
    
    Mirf.setRADDR((byte *)"serv1");
    
    /*
     * Set the payload length to sizeof(unsigned long) the
     * return type of millis().
     *
     * NB: payload on client and server must be the same.
     */
    
    Mirf.payload = sizeof(unsigned long);
    
    /*
     * Write channel and payload config then power up reciver.
     */
    
    Mirf.config();
    
    
    Bridge.begin();
    
    //Serial.println("Listening...");
}


void showRGB(byte r, byte g, byte b)
{
    analogWrite(PIN_RGB_R, r);
    analogWrite(PIN_RGB_G, g);
    analogWrite(PIN_RGB_B, b);
}

int response = CODE_AVAILABLE;
int lastResponse = response;

double fadeRadians = 0.0;
float factor = 1.0;

float availableR = 0.0;
float availableG = 255.0;
float availableB = 0.0;

float occupiedR = 255.0;
float occupiedG = 0.0;
float occupiedB = 0.0;

float panicR = 255.0;
float panicG = 0.0;
float panicB = 255.0;

double blinkSpeed = .1;
double blinkSpeedMin = .05;
double blinkSpeedMax = .6;

unsigned long lastUnvailableTime = 0;
unsigned long lastUpdateTime = 0;
unsigned long lastChangedStatusTime = 0;
unsigned long timeSinceLastUpdate = 0;
unsigned long timeSinceLastStatusChange = 0;
bool isConnected = false;

void loop()
{
    unsigned long now = millis();
    
    /*
     * A buffer to store the data.
     */
    
    byte data[Mirf.payload];
    
    /*
     * If a packet has been recived.
     *
     * isSending also restores listening mode when it
     * transitions from true to false.
     */
    
    if (!Mirf.isSending() && Mirf.dataReady()) {
        // Serial.println("Got packet");
        
        lastUpdateTime = now;
        
        /*
         * Get load the packet into the buffer.
         */
        
        Mirf.getData(data);
        
        
        /*
         * Set the send address.
         */
        
        
        Mirf.setTADDR((byte *)"clie1");
        
        /*
         * Send the data back to the client.
         */
        delay(10);
        Mirf.send(data);
        
        /*
         * Wait untill sending has finished
         *
         * NB: isSending returns the chip to receving after returning true.
         */
        response = data[0];
        
        
        //When changes to available
        timeSinceLastStatusChange = now - lastChangedStatusTime;
        if (lastResponse != response && timeSinceLastStatusChange > 15000) {
            lastResponse = response;
            lastChangedStatusTime = now;
            if (response == CODE_AVAILABLE) {
                if (lastUnvailableTime > 0)
                {
                    float elapsedTime = (((float) (now - lastUnvailableTime)) / 1000.0);
                    String timeString = "";
                    if (elapsedTime >= 60)
                    {
                        int minutes = (int) (elapsedTime / 60.0);
                        timeString = String(minutes) + "min%20";
                        elapsedTime = elapsedTime - (minutes * 60);
                    }
                    timeString = timeString + String(elapsedTime) + "sec";
                    
                    String command = "curl -sS -k -d 'auth_token=1502c963c7625fbad35da0e9781ea3&room_id=537554&from=DumpLogger&color=green&message_format=html&message=The%20toilet%20is%20available%20(" + timeString  + ")&notify=0' https://api.hipchat.com/v1/rooms/message";
                    hipChatProcess.runShellCommand(command);
                }
            } else if (response == CODE_UNAVAILABLE) {
                hipChatProcess.runShellCommand("curl -sS -k -d 'auth_token=1502c963c7625fbad35da0e9781ea3&room_id=537554&from=DumpLogger&color=red&message_format=html&message=The%20toilet%20is%20unavailable&notify=0' https://api.hipchat.com/v1/rooms/message");
                lastUnvailableTime = now;
            }
        }
        
        /*
         Serial.println(data[0], DEC);
         Serial.println(data[1], DEC);
         Serial.println(data[2], DEC);
         Serial.println(data[3], DEC);
         
         Serial.println("Reply sent.");
         */
    } //end of got data
    
    factor = 0.5 + 0.5 * ((cos(fadeRadians) + 1.0) / 2.0);
    
    float fAR = factor * availableR;
    float fAG = factor * availableG;
    float fAB = factor * availableB;
    
    float fOR = factor * occupiedR;
    float fOG = factor * occupiedG;
    float fOB = factor * occupiedB;
    
    timeSinceLastUpdate = now - lastUpdateTime;
    //  if (timeSinceLastUpdate > 40000) {
    //    isConnected = false;
    //    showRGB((byte)panicR, (byte)panicG, (byte)panicB);
    //    blinkSpeed = blinkSpeedMax;
    //  } else {
    //    if(!isConnected){
    //      isConnected = true;
    //      blinkSpeed = blinkSpeedMin;
    //    }
    
    if (response == CODE_AVAILABLE)
    {
        showRGB((byte)fAR, (byte)fAG, (byte)fAB);
        blinkSpeed -= .0003;
    }
    else if (response == CODE_UNAVAILABLE)
    {
        showRGB((byte)fOR, (byte)fOG, (byte)fOB);
        blinkSpeed += .00015;
    }
    //  }
    
    blinkSpeed = max(min(blinkSpeed, 0.6), 0.05);
    fadeRadians += blinkSpeed;
    
    delay(30);
}